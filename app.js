var log = require('color-logs')(true, true, __filename);
var express = require('express');
var app = express();

var SearchTool = require('./search');
var search = new SearchTool();

// cross domain:
// Add headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
}); 


app.get('/search/:name', function(request, response){
	response.writeHead(200, {"Content-Type": "text/json"});

	var query = request.params.name;
	log.Colors("white", "yellow").Debug("Looking for " + query);

	search.SearchList(query, function(result){
		response.write(JSON.stringify(result));
		response.end();
	});
}.bind(this));

app.get('/info/cerveja*', function(request, response){
	response.writeHead(200, {"Content-Type": "text/json"});

	var query = request.params[0];
	console.log("Getting beer info for " + query);

	search.SearchBeer(query, function(result){
		console.info(result);
		response.write(JSON.stringify(result));
		response.end();
	});

});

try{
	var port = process.env.PORT || 3000;
	var server = app.listen(port, function(){
		var host = server.address().address;
		var port = server.address().port;

		log.Debug("App listening at //%s:%s", host, port);
	});
} catch(ex){
	log.Error(ex);
}
