var request = require("request");
var jsdom = require("jsdom");
var $ = require("jquery");

function searchTool(){
	function beerListItem(name, link){
		this.name = name;
		this.link = link;
	}
	function beerDetails(){
		this.name = null;
		this.brewery = null;
		this.style = null;
		this.alcohol = null;
		this.ingredients = null;
		this.description = null;
		this.link = null;
	}

	function searchListPage(beerName, page, callback){
		var options = {
			host: 'www.brejas.com.br',
			path: '/busca/resultados-busca?dir=1&keywords=' + beerName + "&page=" + page
		};
		var url = 'http://' + options.host + options.path;
		console.info("calling " + url);
		request({ uri: url }, function(error, response, body){
			if(error || response.statusCode != 200){
				console.log("ERROR!");
			}

			jsdom.env({
				html: body,
				scripts: [
					'http://code.jquery.com/jquery-1.5.min.js'
				],
				done: function(err, window){
		    		var $ = window.jQuery;
		    		var bList = $('.jrContentTitle a');
		    		var result = [];
		    		for( var i=0; i<bList.size(); i++ ){
			    		item = bList[i];
			    		result[i] = new beerListItem($(item).html(), $(item).attr("href"));
		    		}
		    		var paginationTxt = $('.jrPagenavResults').html();
		    		var pagination = paginationTxt.split(" ")[0];
		    		pagination = Math.ceil(pagination/10);
		    		console.info("pagination: " + pagination + ", page: " + page);
		    		if(pagination == page){
			    		callback(result);
		    		} else{
						searchListPage(beerName, (page+1), function(rs){
							callback(result.concat(rs));
						});			    			
		    		}
	    		}
	    	});
		});

	}
	function searchList(beerName, callback){
		var result = searchListPage(beerName, 1, function(result){
			callback(result);
		});
	}

	function getBeerDetails(link, callback){
		var options = {
			host: 'www.brejas.com.br/',
			path: 'cerveja' + link
		};
		var url = 'http://' + options.host + options.path;
		console.info("calling " + url);
		request({ uri: url }, function(error, rsp, body){
			if(error || rsp.statusCode != 200){
				console.log("ERROR!");
			}

			jsdom.env({
				html: body,
				scripts: [
					'http://code.jquery.com/jquery-1.5.min.js'
				],
				done: function(err, window){
					var beer = new beerDetails();

					var $ = window.jQuery;
					beer.name = $("h1.contentheading").find("span:first").html();
					beer.link = options.path;
					beer.brewery = $(".jrCervejaria .jrFieldValue span").html();
					beer.style = $(".jrEstilo .jrFieldValue").find("a:first").html();
					beer.alcohol = $(".jrAlcool .jrFieldValue").html();
					beer.ingredients = $(".jrIngredientes .jrFieldValue").html();
					beer.description = $(".jrListingFulltext").html();

					callback(beer);
	    		}
	    	});
		});
	}

	return {
		SearchList: searchList,
		SearchBeer: getBeerDetails
	}

}

module.exports = searchTool;